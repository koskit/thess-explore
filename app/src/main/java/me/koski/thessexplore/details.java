package me.koski.thessexplore;
import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.content.Intent;
import android.widget.*;
import java.net.URL;
import java.net.HttpURLConnection;
import java.io.InputStream;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.net.MalformedURLException;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.R.id;

public class details extends Activity  {
    Button load_img;
    ImageView img;
    Bitmap bitmap;
    ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details);

        Intent iin= getIntent();
        Bundle b = iin.getExtras();

        if(b!=null)
        {
            ((TextView) findViewById(R.id.title)).setText((String) b.get(ReadComments.TAG_TITLE));
            ((TextView) findViewById(R.id.description)).setText((String) b.get(ReadComments.TAG_DESCRIPTION));
        }
        String image_web_url = (String) b.get (ReadComments.TAG_IMG);
        img = (ImageView)findViewById(R.id.img);

        new LoadImage().execute(image_web_url);
    }

    private class LoadImage extends AsyncTask<String, String, Bitmap> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(details.this);
            pDialog.setMessage("Loading Image ....");
            pDialog.show();
        }

        protected Bitmap doInBackground(String... args) {
            try {
                bitmap = BitmapFactory.decodeStream((InputStream)new URL(args[0]).getContent());

            }
            catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap image) {

            if(image != null){
                img.setImageBitmap(image);
                pDialog.dismiss();
            }
            else{
                pDialog.dismiss();
                Toast.makeText(details.this, "Image Does Not exist or Network Error", Toast.LENGTH_SHORT).show();
            }
        }
    }

}