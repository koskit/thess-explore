package me.koski.thessexplore;

import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

public class ReadComments extends ListActivity {

	// Progress Dialog
	private ProgressDialog pDialog;

		// testing on Emulator:
	private static final String READ_COMMENTS_URL = "http://thessexplore.viewpanel.gr/sightseeing.php";

	// JSON IDS:
	public static final String TAG_SUCCESS = "success";
	public static final String TAG_TITLE = "title";
	public static final String TAG_SIGHTSEEING = "sightseeing";
	public static final String TAG_SS_ID = "ss_id";
	public static final String TAG_LINK = "link";
	public static final String TAG_DESCRIPTION = "description";
	public static final String TAG_IMG = "img";


	// An array of all of our comments
	private JSONArray mComments = null;
	// manages all of our comments in a list.
	private ArrayList<HashMap<String, String>> mCommentList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// note that use read_comments.xml instead of our single_post.xml
		setContentView(R.layout.read_comments);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		// loading the comments via AsyncTask
		new LoadComments().execute();
	}


	/**
	 * Retrieves recent post data from the server.
	 */
	public void updateJSONdata() {

		mCommentList = new ArrayList<HashMap<String, String>>();


		JSONParser jParser = new JSONParser();
		JSONObject json = jParser.getJSONFromUrl(READ_COMMENTS_URL);

		// try to catch any exceptions:
		try {

			mComments = json.getJSONArray(TAG_SIGHTSEEING);

			// looping through all posts according to the json object returned
			for (int i = 0; i < mComments.length(); i++) {
				JSONObject c = mComments.getJSONObject(i);

				// gets the content of each tag
				String title = c.getString(TAG_TITLE);
				String description = c.getString(TAG_DESCRIPTION);
				String img = c.getString(TAG_IMG);
				String link = c.getString(TAG_LINK);

				// creating new HashMap
				HashMap<String, String> map = new HashMap<String, String>();

				map.put(TAG_SS_ID, c.getString(TAG_SS_ID));
				map.put(TAG_TITLE, title);
				map.put(TAG_DESCRIPTION, description);
				map.put(TAG_IMG, img);
				map.put(TAG_LINK, link);

				// adding HashList to ArrayList
				mCommentList.add(map);

			}

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Inserts the parsed data into the listview.
	 */
	private void updateList() {

		ListAdapter adapter = new SimpleAdapter(this, mCommentList,
				R.layout.single_post, new String[] { TAG_TITLE}, new int[] { R.id.title});


		setListAdapter(adapter);
		

		ListView lv = getListView();	
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id)  {

				// ListView Clicked item index
				int itemPosition     = position;
				ListView lv1 = getListView();
				// ListView Clicked item value
				HashMap  itemValue    = (HashMap) lv1.getItemAtPosition(position);

				Intent startDetails = new Intent(getApplicationContext(), details.class);
				startDetails.putExtra (TAG_SS_ID, (String)itemValue.get(TAG_SS_ID));
				startDetails.putExtra(TAG_TITLE, (String)itemValue.get(TAG_TITLE));
				startDetails.putExtra(TAG_DESCRIPTION, (String)itemValue.get(TAG_DESCRIPTION));
				startDetails.putExtra(TAG_IMG, (String)itemValue.get(TAG_IMG));
				startDetails.putExtra(TAG_LINK, (String)itemValue.get(TAG_LINK));
				startActivity(startDetails);
				// Show Alert
				/*Toast.makeText(getApplicationContext(),
						"Position :"+itemPosition+"  ListItem : " +itemValue , Toast.LENGTH_LONG)
						.show();
				*/
			}
		});
	}

	public class LoadComments extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(ReadComments.this);
			pDialog.setMessage("Loading...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		@Override
		protected Boolean doInBackground(Void... arg0) {
			updateJSONdata();
			return null;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			pDialog.dismiss();
			updateList();
		}
	}
}
