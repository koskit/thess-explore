package me.koski.thessexplore;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class home extends Activity implements OnClickListener {

    private Button myAccount;
    private Button sightaActivity;
    private Button eventsActivity;
    private Button favoritedActivity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);

        myAccount = (Button) findViewById(R.id.myaccount);
        sightaActivity = (Button) findViewById(R.id.sightButton);
        eventsActivity = (Button) findViewById(R.id.eventsButton);
        favoritedActivity = (Button) findViewById(R.id.favotitesButton);

        myAccount.setOnClickListener(this);
        sightaActivity.setOnClickListener(this);
        eventsActivity.setOnClickListener(this);
        favoritedActivity.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.myaccount:
                Intent changeToMyAccount = new Intent(this, Login.class);
                startActivity(changeToMyAccount);
                break;
            case R.id.sightButton:
                Intent changeToSightSeeing = new Intent(this, ReadComments.class);
                startActivity(changeToSightSeeing);
                break;
            case R.id.eventsButton:
                Intent changeToEvents = new Intent(this, feed.class);
                startActivity(changeToEvents);
                break;
            case R.id.favotitesButton:
                // TODO go to Favorites
                break;
            default:
                break;
        }
    }
}
