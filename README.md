# README #
### What is this repository for? ###
This repository holds a project made for a Class project at University of Macedonia / IT Department back in 2015. 
Version: 1.0.0

### How do I get set up? ###
Just download the whole repository and insert it to an android studio project. It's been some time, so I don't know if you will be able to build the project and run it on a virtual/real smartphone. Some build settings might need changes to be able to run at newer versions.

### Contribution guidelines ###
Nothing to contribute I'm afraid, project got discontinued after presenting the assignment, and it was my first experience with android studio / coding for smartphones, back then. The code consists of some classes taken from public gits to fill the functional needs, and some custom code to tailor them to the project's needs.

### Who do I talk to? ###
If you wish to contact me, send an e-mail at: koski.t.k(at)gmail.com

### Additional info ###
You can find some screenshots in the master branch, file name "Screenshots" (original HA!), if you wish to take a look at the app.